public interface IGraphicSettings  {
    int getNeededProcessingPower();
    void processFrame(int[][] ramka);
}
