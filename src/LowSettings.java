public class LowSettings implements IGraphicSettings {

    @Override
    public int getNeededProcessingPower() {
        return 100;
    }
}
